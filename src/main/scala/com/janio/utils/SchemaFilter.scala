package com.janio.utils

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._

object SchemaFilter {

  def filterTypes(df: DataFrame) = {
    df.dtypes.collect {
      case (dn, dt) if dt.startsWith("TimestampType") => {
        (dn, TimestampType)
      }
      case (dn, dt) if dt.startsWith("IntegerType") => {
        (dn, IntegerType)
      }
      case (dn, dt) if dt.startsWith("DateType") => (dn, DateType)
      case (dn, dt) if dt.startsWith("BooleanType") => (dn, BooleanType)
      case (dn, dt) if dt.startsWith("LongType") => (dn, LongType)
      case (dn, dt) if dt.startsWith("DoubleType") => (dn, DoubleType)
      case (dn, dt) if dt.startsWith("DecimalType") => (dn, FloatType)
    }.foldLeft(df)((accDF, c) => accDF.withColumn(c._1, col(c._1).cast(c._2)))
    df
  }

}

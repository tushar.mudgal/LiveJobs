package com.janio.jobs

import com.janio.connection.SparkInstance
import com.janio.dao.JanioBackendSchema
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.{DataTypes, DateType, DoubleType, LongType, TimestampType}
import org.slf4j.{Logger, LoggerFactory}

object LiveJobsRunner {
  val infoLogger: Logger = LoggerFactory.getLogger(LiveJobsRunner.getClass)
  val config: Config = ConfigFactory.load("config")

  def main(args: Array[String]): Unit = {
    val spark = SparkInstance.getSparkInstance()

    /**
     * Creating dataframe from kafka
     */
    import spark.implicits._
    val df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", config.getString("KAFKA_BOOTSTRAP_SERVERS"))
      .option("subscribe", config.getString("KAFKA_TOPIC"))
      .option("startingOffsets", "earliest")
      .load()
      .selectExpr("CAST(value AS STRING)", "offset")
      .as[(String, Long)]
      .withColumn("source", get_json_object(col("value"), "$.source"))
      .withColumn("table", get_json_object(col("source"), "$.table"))
      .withColumn("event_timestamp", get_json_object(col("value"), "$.ts_ms").$div(1000)
        .cast(TimestampType))
      .withColumn("lsn", get_json_object(col("source"), "$.lsn").cast(LongType))
      .withColumn("event_type", when(get_json_object(col("value"), "$.before").isNull === true, "insert")
        .when(get_json_object(col("value"), "$.before").isNotNull === true
          && get_json_object(col("value"), "$.after").isNotNull === true, "update")
        .when(get_json_object(col("value"), "$.after").isNull === true, "delete"))
      .withColumn("value", when(get_json_object(col("value"), "$.after").isNull === true, get_json_object(col("value"), "$.before"))
        .otherwise(get_json_object(col("value"), "$.after")))
      .withColumn("j_date", to_date(col("event_timestamp"), "yyyy-MM-dd"))
      .drop("source")


    /**
     * Writing data for janio_order_order table
     */
    val janio_order_order = df.filter("table='janio_order_order'")
      .withColumn("value", from_json(col("value"),
        JanioBackendSchema.janioOrderOrder)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .withColumn("updated_on", $"updated_on".cast(TimestampType))
      .withColumn("submit_external_service_datetime", $"submit_external_service_datetime".cast(TimestampType))
      .withColumn("submit_firstmile_datetime", $"submit_firstmile_datetime".cast(TimestampType))
      .withColumn("submit_warehouse_datetime", $"submit_warehouse_datetime".cast(TimestampType))
      .withColumn("private_tracker_updated_on", $"private_tracker_updated_on".cast(TimestampType))
      .withColumn("tracker_updated_on", $"tracker_updated_on".cast(TimestampType))
      .withColumn("est_completion_date", $"est_completion_date".cast(TimestampType))
      .withColumn("est_early_completion_date", $"est_early_completion_date".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_order/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_order/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    /**
     * Writing data for janio_tracker_update table
     */
    val janio_tracker_update = df.filter("table='janio_tracker_update'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioTrackerUpdate)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .withColumn("updated_on", $"updated_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_tracker_update/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_tracker_update/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    /**
     * Writing data for janio_order_agentapplication table
     */
    val janio_order_agentapplication = df.filter("table='janio_order_agentapplication'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioOrderAgentapplication)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_agentapplication/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_agentapplication/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    /**
     * Writing data for janio_tracker_privateupdate table
     */
    val janio_tracker_privateupdate = df.filter("table='janio_tracker_privateupdate'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioTrackerPrivateUpdate)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("updated_on", $"updated_on".cast(TimestampType))
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_tracker_privateupdate/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_tracker_privateupdate/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    /**
     * Writing data for janio_order_service table
     */
    val janio_order_service = df.filter("table='janio_order_service'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioOrderService)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_service/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_service/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    /**
     * Writing data for janio_payment_paymentsettings table
     */
    val janio_payment_paymentsettings = df.filter("table='janio_payment_paymentsettings'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioPaymentPaymentSettings)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_payment_paymentsettings/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_payment_paymentsettings/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_payment_paymentsettings_pricing_groups table
     */
    val janio_payment_paymentsettings_pricing_groups = df.filter("table='janio_payment_paymentsettings_pricing_groups'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioPaymentSettingsPricingGroups)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_payment_paymentsettings_pricing_groups/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_payment_paymentsettings_pricing_groups/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_payment_pricinggroup table
     */
    val janio_payment_pricinggroup = df.filter("table='janio_payment_pricinggroup'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioPaymentPricingGroup)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_payment_pricinggroup/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_payment_pricinggroup/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_partner_partner table
     */
    val janio_partner_partner = df.filter("table='janio_partner_partner'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioPartnerPartner)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_partner_partner/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_partner_partner/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_order_item table
     */
    val janio_order_item = df.filter("table='janio_order_item'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioOrderItem)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_item/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_item/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_order_orderpickupdetails table
     */
    val janio_order_orderpickupdetails = df.filter("table='janio_order_orderpickupdetails'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioOrderOrderPickUpDetails)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("pickup_date", expr("date_add('1970-01-01',pickup_date)"))
      .withColumn("pickup_time_from", date_format($"pickup_time_from".$div(1000000).cast(DataTypes.TimestampType), "HH:mm:ss"))
      .withColumn("pickup_time_to", date_format($"pickup_time_to".$div(1000000).cast(DataTypes.TimestampType), "HH:mm:ss"))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_orderpickupdetails/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_orderpickupdetails/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_analytics_ordertrackerinfoanalytics table
     */
    val janio_analytics_ordertrackerinfoanalytics = df.filter("table='janio_analytics_ordertrackerinfoanalytics'")
      .withColumn("value", from_json(col("value"),
        JanioBackendSchema.janioAnalyticsOrderTrackerInfoAnalytics)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("analytics_created_on", $"analytics_created_on".cast(TimestampType))
      .withColumn("analytics_updated_on", $"analytics_updated_on".cast(TimestampType))
      .withColumn("attempt_1_on", $"attempt_1_on".cast(TimestampType))
      .withColumn("attempt_2_on", $"attempt_2_on".cast(TimestampType))
      .withColumn("attempt_3_on", $"attempt_3_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_analytics_ordertrackerinfoanalytics/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_analytics_ordertrackerinfoanalytics/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_sla_orderslazone table
     */
    val janio_sla_orderslazone = df.filter("table='janio_sla_orderslazone'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioSLAOrdersLAZone)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_sla_orderslazone/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_sla_orderslazone/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_location_zone table
     */
    val janio_location_zone = df.filter("table='janio_location_zone'").withColumn("value", from_json(col("value"),
      JanioBackendSchema.janioLocationZone)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_location_zone/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_location_zone/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_order_orderzone table
     */
    val janio_order_orderzone = df.filter("table='janio_order_orderzone'")
      .withColumn("value", from_json(col("value"),
        JanioBackendSchema.janioOrderOrderZone)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_order_orderzone/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_order_orderzone/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    /**
     * Writing data for janio_cod_orderremittancedata table
     */
    val janio_cod_orderremittancedata = df.filter("table='janio_cod_orderremittancedata'")
      .withColumn("value", from_json(col("value"),
        JanioBackendSchema.janioCodOrderRemittanceData)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_cod_orderremittancedata/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_cod_orderremittancedata/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()

    val janio_sla_orderpartnerdelay = df.filter("table='janio_sla_orderpartnerdelay'")
      .withColumn("value", from_json(col("value"),
        JanioBackendSchema.janioSlaOrderpartnerdelay)).select("value.*", "event_type", "event_timestamp", "j_date", "lsn")
      .withColumn("created_on", $"created_on".cast(TimestampType))
      .withColumn("updated_on", $"updated_on".cast(TimestampType))
      .withColumn("no_of_days_delayed", $"no_of_days_delayed".cast(DoubleType))
      .withColumn("original_estimated_completion_date", $"original_estimated_completion_date".cast(DateType))
      .withColumn("revised_estimated_completion_date", $"revised_estimated_completion_date".cast(DateType))
      .repartition(1)
      .writeStream
      .outputMode("append")
      .option("path", config.getString("S3_WRITE_PATH") + "janio_sla_orderpartnerdelay/")
      .option("checkpointLocation", config.getString("S3_KAFKA_CHECKPOINT") + "janio_sla_orderpartnerdelay/")
      .option("orc.compress", "zlib")
      .format("orc")
      .partitionBy("j_date")
      .trigger(Trigger.ProcessingTime(config.getString("SCHEDULE_INTERVAL")))
      .start()


    janio_order_order.awaitTermination()
    janio_tracker_update.awaitTermination()
    janio_order_agentapplication.awaitTermination()
    janio_tracker_privateupdate.awaitTermination()
    janio_order_service.awaitTermination()
    janio_payment_paymentsettings.awaitTermination()
    janio_payment_paymentsettings_pricing_groups.awaitTermination()
    janio_payment_pricinggroup.awaitTermination()
    janio_partner_partner.awaitTermination()
    janio_order_item.awaitTermination()
    janio_order_orderpickupdetails.awaitTermination()
    janio_analytics_ordertrackerinfoanalytics.awaitTermination()
    janio_sla_orderslazone.awaitTermination()
    janio_location_zone.awaitTermination()
    janio_order_orderzone.awaitTermination()
    janio_cod_orderremittancedata.awaitTermination()
    janio_sla_orderpartnerdelay.awaitTermination()
  }
}
